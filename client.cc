/*
 * NarupaClientLib Testing Zone
 */

#include <iostream>
#include <thread>
#include "NarupaClient.h"

int main (int argc, char** argv)
{
    TrajectoryService trajectoryService;
    CommandService commandService;

    trajectoryService.setPositionCallback ([](std::vector<float> positions)
                                           {
                                              for (auto p : positions)
                                            {
                                                std::cout << p << std::endl;
                                            }
                                          });

    std::cout << "Should see frames" << std::endl;
    
    while (true)
        std::this_thread::sleep_for(std::chrono::milliseconds (200));

  return 0;
}
