/*
 * Copyright (c) 2020 Tom Mitchell.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ImdService.h"


#include <grpcpp/grpcpp.h>
#include "narupa/protocol/imd/imd.grpc.pb.h"

class ImdMembers
{
public:
    ImdMembers()
    {
        //connect ("130.61.91.189");
    }

    bool connect (const std::string& ipAddress)
    {
        auto ipAndPort = ipAddress + ":" + std::to_string (38801);
        channel = grpc::CreateChannel (ipAndPort, grpc::InsecureChannelCredentials());
        imdStub = narupa::protocol::imd::InteractiveMolecularDynamics::NewStub (channel);
        return true;
    }

    ~ImdMembers()
    {

    }


    void beginOrUpdateInteration (ImdService::Interaction& interaction)
    {
        if (streams.find (interaction.id) == streams.end()) //if this is a new interaction create a stream
        {
            auto& stream = streams[interaction.id];
            stream.context = std::make_unique<grpc::ClientContext>();
            stream.writer = imdStub->PublishInteraction (stream.context.get(), &(stream.interactionEndReply));
        }

        narupa::protocol::imd::ParticleInteraction particleInteraction;
        particleInteraction.set_interaction_id (interaction.id);
        particleInteraction.add_particles (interaction.particleIndex);
        particleInteraction.add_position (interaction.position[0]);
        particleInteraction.add_position (interaction.position[1]);
        particleInteraction.add_position (interaction.position[2]);
        
        auto propertyFields = particleInteraction.mutable_properties()->mutable_fields();
        
        google::protobuf::Value value;
        value.set_number_value (interaction.scale);
        (*propertyFields)[ScaleKey] = value;
        
        value.set_string_value (TypeValueSpring);
        (*propertyFields)[TypeKey] = value;
        
        if (! streams[interaction.id].writer->Write (particleInteraction))
            std::cout << "We have a problem";
    }

    void endInteraction(const std::string interactionId)
    {
        if (streams.find (interactionId) != streams.end())
        {
            auto& stream = streams[interactionId];
            stream.writer->WritesDone();
            grpc::Status status = stream.writer->Finish();
            streams.erase (interactionId);
            if (!status.ok())
                std::cout << "Stream status error" << std::endl;
        }
    }

private:
    std::shared_ptr<grpc::Channel> channel;
    std::unique_ptr<narupa::protocol::imd::InteractiveMolecularDynamics::Stub> imdStub;

    const std::string ImdMembers::ScaleKey          { "scale" };
    const std::string ImdMembers::TypeKey           { "type" };
    const std::string ImdMembers::TypeValueSpring   { "spring" };
    const std::string ImdMembers::TypeValueGaussian { "gaussian" };

    struct ImdStream
    {      
        narupa::protocol::imd::InteractionEndReply interactionEndReply;
        std::unique_ptr<grpc::ClientContext> context;
        std::unique_ptr<grpc::ClientWriter<narupa::protocol::imd::ParticleInteraction>> writer;
    };
    std::map <const std::string, ImdStream> streams;
};

ImdService::ImdService()
{
    members = std::make_unique <ImdMembers>();
}

ImdService::~ImdService()
{

}

bool ImdService::connect (const std::string& ipAddress)
{
    return members->connect (ipAddress);
}

void ImdService::beginOrUpdateInteration (Interaction& interaction)
{
    members->beginOrUpdateInteration (interaction);
}

void ImdService::endInteraction (const std::string& interactionId)
{
    members->endInteraction (interactionId);
}
