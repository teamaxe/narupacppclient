/*
 * Copyright (c) 2020 Tom Mitchell.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#pragma once

#include <vector>
#include <functional>
#include <memory>

class TrajectoryMembers;

class TrajectoryService
{
public:
    TrajectoryService();
    ~TrajectoryService();
    bool connect (const std::string& ipAddress);
    void stop();

    void setPositionCallback (std::function<void (std::vector<float>)> callback);
    void setKinEnergyCallback (std::function<void (float kinEnergy)> callback);
    void setChainCountCallback (std::function<void (int chainCount)> callback);
    void setNonBondedInteractionCallback (std::function <void (int first, int second, double value)> callback);
    void setFrameEndCallback(std::function <void(void)> callback);

private:
    std::unique_ptr <TrajectoryMembers> members;
};
