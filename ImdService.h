/*
 * Copyright (c) 2020 Tom Mitchell.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#pragma once

#include <string>
#include <array>
#include <vector>
#include <memory>
#include <map>

class ImdMembers;

class ImdService
{
public:
    ImdService();
    ~ImdService();

    bool connect (const std::string& ipAddress);

    struct Interaction
    {
        enum class ForceType
        {
            Spring,
            Gaussian
        };

        Interaction (const std::string& id_, const std::array<float, 3>& p, int pi, float s, ForceType t)
         :  id (id_), position (p), particleIndex (pi), scale (s), type (t) {}
        std::string id;
        std::array<float, 3> position = {0.f, 0.f, 0.f};
        int particleIndex { 0 };
        float scale { 100.f };
        ForceType type { ForceType::Spring };
    };

    void beginOrUpdateInteration (Interaction& interaction);
    void endInteraction (const std::string& interactionId);

private:
    std::unique_ptr <ImdMembers> members;
};
