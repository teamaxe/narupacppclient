/*
 * Copyright (c) 2020 Tom Mitchell.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TrajectoryService.h"

#include <string>
#include <thread>
#include <atomic>
#include <mutex>
#include <grpcpp/grpcpp.h>
#include <chrono>
#include "narupa/protocol/trajectory/trajectory_service.grpc.pb.h"
#include <windows.h>
#include <string_view>
#include <charconv>

static const std::string BondPairs          = { "bond.pairs" };          //array<uint32> pairs of bonded atoms
static const std::string ChainCount         = { "chain.count" };         //int number of individual chains
static const std::string ChainNames         = { "chain.names" };         //array<strings> name of above chains
static const std::string EnergyKinetic      = { "energy.kinetic" };      //float system's kinetic energy
static const std::string ParticleCount      = { "particle.count" };      //float number atoms in the system
static const std::string ParticleElements   = { "particle.elements" };   //array<uint32> index in the peroidic table
static const std::string ParticleNames      = { "particle.names" };      //array<string> array of all atom names
static const std::string ParticlePositions  = { "particle.positions" };  //array<float> position of each atom
static const std::string ParticleResidues   = { "particle.residues" };   //array<uint32> residue to which each atom belongs
static const std::string ResidueChains      = { "residue.chains" };      //array<uint32>
static const std::string ResidueCount       = { "residue.count" };       //
static const std::string ResidueIds         = { "residue.ids" };         //array<strings> names of residues
static const std::string SystemBoxVectors   = { "system.box.vectors" };  //array<float> 9 - unit cell vector for the constraints

struct TrajectoryState
{
    void setParticleCount(int newNumberOfParticles)
    {
        if (numberOfParticles != newNumberOfParticles)
        {
            numberOfParticles = newNumberOfParticles;
            positions.resize (numberOfParticles, 0.f);
        }
    }
    int getParticleCount() const
    {
        return numberOfParticles;
    }

    std::vector<float>& getPositionsReference()
    {
        return positions;
    }

    float& getKinEnergyReference()
    {
        return kinEnergy;
    }

    int& getChainCountReference()
    {
        return chainCount;
    }

    void setKinEnergy(float ke)
    {
        kinEnergy = ke;
    }

    void setChainCount(float cc)
    {
        chainCount = cc;
    }

    bool isPopulated() const
    {
        return numberOfParticles != 0;
    }
private:
    int numberOfParticles{ 0 };
    float kinEnergy = 0;
    int chainCount = 0;
    std::vector<float> positions;
};

class TrajectoryMembers
{
public:
    TrajectoryMembers()
    {
        //connect ("130.61.91.189");
    }

    bool connect (const std::string& ipAddress)
    {
        auto ipAndPort = ipAddress + ":" + std::to_string (38801);
        channel = grpc::CreateChannel (ipAndPort, grpc::InsecureChannelCredentials());
        trajectoryStub = narupa::protocol::trajectory::TrajectoryService::NewStub (channel);
        state = std::make_unique <TrajectoryState>();
        thread = std::thread (&TrajectoryMembers::run, this);
        return true;
    }

    ~TrajectoryMembers()
    {
        if (trajectoryStub)
            stop();
    }

    void stop()
    {
        shouldExit = true;
        thread.join();
    }

    void setPositionCallback (std::function < void(std::vector< float >) > callback)
    {
        std::lock_guard<std::mutex> locker (callbacksLock);
        onPositionUpdate = std::move (callback);
    }

    void setKinEnergyCallback(std::function <void(float)> callback)
    {
        std::lock_guard<std::mutex> locker (callbacksLock);
        onKinEnergyUpdate = std::move (callback);
    }

    void setChainCountCallback(std::function<void(int chainCount)> callback)
    {
        std::lock_guard<std::mutex> locker (callbacksLock);
        onChainCountUpdate = std::move (callback);
    }

    void setNonBondedInteractionCallback(std::function < void(int first, int second, double value) > callback)
    {
        std::lock_guard<std::mutex> locker(callbacksLock);
        onNonBondedInteractionUpdate = std::move (callback);
    }

    void setFrameEndCallback (std::function <void(void)> callback)
    {
        std::lock_guard<std::mutex> locker(callbacksLock);
        onFrameEnd = std::move (callback);
    }

private:
    void run()
    {
        subscribeToFrames();
    }

    void subscribeToFrames()
    {
        grpc::ClientContext context;
        narupa::protocol::trajectory::GetFrameResponse frameResponse;

        std::unique_ptr<grpc::ClientReader<narupa::protocol::trajectory::GetFrameResponse>> reader
        (trajectoryStub->SubscribeLatestFrames (&context, narupa::protocol::trajectory::GetFrameRequest()));
        while (reader->Read (&frameResponse) && shouldExit == false)
        {
            auto& frame = frameResponse.frame();
            
            //for (auto d : frame.arrays())
            //{
            //    std::cout << "arrays:" << d.first << std::endl;
            //}
            for (auto d : frame.values())
            {
                static std::string nonBondedKeyStart ("nonbonded.");
                if (d.first.rfind (nonBondedKeyStart, 0) == 0)
                {
                    //extract pairwise force interactions
                    std::string_view sv (d.first.c_str(), d.first.size());
                    sv.remove_prefix (nonBondedKeyStart.size());
                    int first;
                    if (auto [p, ec] = std::from_chars (sv.data(), sv.data() + sv.size(), first); ec == std::errc())
                    {
                        if (auto pos = sv.find_first_of ('.'); pos != std::string::npos && pos + 1 < sv.size())
                        {
                            sv.remove_prefix (pos + 1);
                            int second;
                            if (auto [p, ec] = std::from_chars (sv.data(), sv.data() + sv.size(), second); ec == std::errc())
                            {
                                std::lock_guard<std::mutex> locker (callbacksLock);
                                if (onNonBondedInteractionUpdate)
                                {
                                    onNonBondedInteractionUpdate (first, second, d.second.number_value());
                                }
                            }
                        }
                    }
                }
                //std::cout << "values" << d.first << std::endl;
            }

            if (frame.values().contains (ParticleCount))
            {
                auto particleCount = frame.values().at (ParticleCount).number_value();
                state->setParticleCount (particleCount);
            }

            if (frame.arrays().contains (ParticlePositions))
            {
                auto particlePositions = frame.arrays().at (ParticlePositions).float_values().values();
                state->getPositionsReference().assign (particlePositions.begin(), particlePositions.end());
                std::lock_guard<std::mutex> locker (callbacksLock);
                if (onPositionUpdate)
                    onPositionUpdate (state->getPositionsReference());
            }

            if (frame.values().contains (EnergyKinetic))
            {
                auto kinEnergy = frame.values().at (EnergyKinetic).number_value();
                state->setKinEnergy (kinEnergy);
                std::lock_guard<std::mutex> locker (callbacksLock);
                if (onKinEnergyUpdate)
                    onKinEnergyUpdate (state->getKinEnergyReference());
            }

            if (frame.values().contains (ChainCount))
            {
                auto chainCount = frame.values().at (ChainCount).number_value();
                state->setChainCount ((int)chainCount);
                std::lock_guard<std::mutex> locker (callbacksLock);
                if (onChainCountUpdate)
                    onChainCountUpdate (state->getChainCountReference());
            }

            if (std::lock_guard<std::mutex> locker(callbacksLock); onFrameEnd)
            {
                onFrameEnd();
            }

            //uncomment to print out packet rate
//            static int rateCounter = 0;
//            static auto oneSecondLater = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() + 1000;
//            auto currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
//            rateCounter++;
//            if (currentTime >= oneSecondLater)
//            {
//                std::wstring s(L"Rate per sec: " + std::to_wstring (rateCounter) + L"\n");
//                LPCWSTR wide_string; //define an array with size of my_str + 1
//                wide_string = s.c_str();
//                OutputDebugStringW(wide_string);
//                rateCounter = 0;
//                oneSecondLater = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() + 1000;
//            }
        }
    }

    std::unique_ptr <TrajectoryState> state;

    std::thread thread;
    std::atomic<bool> shouldExit{ false };

    std::mutex callbacksLock;
    std::function<void(std::vector<float>)> onPositionUpdate;
    std::function<void(float)> onKinEnergyUpdate;
    std::function<void(int)> onChainCountUpdate;
    std::function <void(int first, int second, double value)> onNonBondedInteractionUpdate;
    std::function<void()> onFrameEnd;

    std::shared_ptr<grpc::Channel> channel;
    std::unique_ptr<narupa::protocol::trajectory::TrajectoryService::Stub> trajectoryStub;
};

TrajectoryService::TrajectoryService()
{
    members = std::make_unique <TrajectoryMembers>();
}

TrajectoryService::~TrajectoryService()
{

}

bool TrajectoryService::connect (const std::string& ipAddress)
{
    return members->connect (ipAddress);
}

void TrajectoryService::stop()
{
    members->stop();
}

void TrajectoryService::setPositionCallback (std::function < void(std::vector< float >) > callback)
{
    members->setPositionCallback (callback);
}

void TrajectoryService::setKinEnergyCallback (std::function <void(float)> callback)
{
    members->setKinEnergyCallback (callback);
}

void TrajectoryService::setChainCountCallback (std::function<void(int chainCount)> callback)
{
    members->setChainCountCallback (callback);
}

void TrajectoryService::setNonBondedInteractionCallback (std::function <void(int first, int second, double value)> callback)
{
    members->setNonBondedInteractionCallback (callback);
}

void TrajectoryService::setFrameEndCallback(std::function <void(void)> callback)
{
    members->setFrameEndCallback (callback);
}
