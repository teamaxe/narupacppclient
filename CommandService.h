/*
 * Copyright (c) 2020 Tom Mitchell.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <vector>
#include <functional>
#include <memory>
#include <string>

class CommandMembers;

class CommandService
{
public:
    CommandService();
    ~CommandService();
    bool connect (const std::string& ipAddress);
//    template<typename FirstArgument, typename... RemainingArguments>
//    bool runCommand (const std::string& commandName, const std::string& argumentName, FirstArgument firstArgument, RemainingArguments... remainingArguments);
    /** used to run commands. You should use the set function over this unless there isn't one for your command. Returns true if successful */


    /** Temperature - range 0 - 10,000. returns true if it successful */
    bool setTemperature (double newTemperature); 
    /** Friction - range 0.01 - 100 returns true if it successful */
    bool setFriction (double newFriction);
    /** Timestep - 0.01 - 1.5 returns true if it successful */
    bool setTimestep (double newTimestep);
    void printCommands();

    bool addNonbondedPair (int first, int second);

private:
    std::unique_ptr <CommandMembers> members;
};