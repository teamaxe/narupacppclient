/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "NarupaClient.h"

#include <grpcpp/grpcpp.h>
#include "narupa/protocol/trajectory/trajectory_service.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

const std::string NarupaClient::Keys::bondPairs         = {"bond.pairs"};
const std::string NarupaClient::Keys::chainCount        = {"chain.count"};
const std::string NarupaClient::Keys::chainNames        = {"chain.names"};
const std::string NarupaClient::Keys::energyKinetic     = {"energy.kinetic"};
const std::string NarupaClient::Keys::particleCount     = {"particle.count"};
const std::string NarupaClient::Keys::particleElements  = {"particle.elements"};
const std::string NarupaClient::Keys::particleNames     = {"particle.names"};
const std::string NarupaClient::Keys::particlePositions = {"particle.positions"};
const std::string NarupaClient::Keys::particleResidues  = {"particle.residues"};
const std::string NarupaClient::Keys::residueChains     = {"residue.chains"};
const std::string NarupaClient::Keys::residueCount      = {"residue.count"};
const std::string NarupaClient::Keys::residueIds        = {"residue.ids"};
const std::string NarupaClient::Keys::systemBoxVectors  = {"system.box.vectors"};

class NarupaStub
{
public:
    NarupaStub() :  stub_(narupa::protocol::trajectory::TrajectoryService::NewStub(grpc::CreateChannel(
                          "localhost:38801", grpc::InsecureChannelCredentials())))
    {

    }
    
    std::unique_ptr<narupa::protocol::trajectory::TrajectoryService::Stub> stub_;
};

struct SimulationState
{
    void setParticleCount(int newNumberOfParticles)
    {
        if (numberOfParticles != newNumberOfParticles)
        {
            numberOfParticles = newNumberOfParticles;
            positions.resize(numberOfParticles, 0.f);
        }
    }
    int getParticleCount() const
    {
        return numberOfParticles;
    }

    std::vector<float>& getPositionsReference()
    {
        return positions;
    }

    float& getKinEnergyReference()
    {
        return kinEnergy;
    }

    int& getChainCountReference()
    {
        return chainCount;
    }

    void setKinEnergy(float ke)
    {
        kinEnergy = ke;
    }

    void setChainCount(float cc) 
    {
        chainCount = cc;
    }

    bool isPopulated() const 
    {
        return numberOfParticles != 0;
    }
private:
    int numberOfParticles{ 0 };
    float kinEnergy = 0;
    int chainCount = 0;
    std::vector<float> positions;
};

NarupaClient::NarupaClient()
{
    state = std::make_unique <SimulationState>();
    stub = std::make_unique <NarupaStub>();
    thread = std::thread (&NarupaClient::run, this);
}

NarupaClient::~NarupaClient()
{
    stop();
    thread.join();
    std::cout << "Thread joined" << std::endl;
}
void NarupaClient::stop()
{
    shouldExit = true;
}

void NarupaClient::setPositionCallback (std::function < void (std::vector< float >) > callback)
{
    std::lock_guard<std::mutex> locker (callbacksLock);
    onPositionUpdate = std::move (callback);
}

void NarupaClient::setKinEnergyCallback(std::function <void(float)> callback)
{
    std::lock_guard<std::mutex> locker(callbacksLock);
    onKinEnergyUpdate = std::move(callback);
}

void NarupaClient::setChainCountCallback(std::function<void(int chainCount)> callback)
{
    std::lock_guard<std::mutex> locker(callbacksLock);
    onChainCountUpdate = std::move(callback);
}

void NarupaClient::run()
{
    subscribeToFrames();
}
void NarupaClient::subscribeToFrames()
{
    ClientContext context;
    narupa::protocol::trajectory::GetFrameResponse frameResponse;

    std::unique_ptr<grpc::ClientReader<narupa::protocol::trajectory::GetFrameResponse>> reader
    (stub->stub_->SubscribeLatestFrames(&context, narupa::protocol::trajectory::GetFrameRequest()));
    while (reader->Read(&frameResponse) && shouldExit == false)
    {
        //std::cout << "Frame" << frameResponse.frame_index() << std::endl;// << " positions: " << frameResponse.frame.arrays['particle.positions']
        auto& frame = frameResponse.frame();

        //            for (auto d : frame.arrays())
        //                std::cout << "arrays:" << d.first << std::endl;
        //            for (auto d : frame.values())
        //                std::cout << "values" << d.first << std::endl;

        auto particleCount = frame.values().find(Keys::particleCount);
        if (particleCount != frame.values().end())
        {
            auto particleCountpt = *particleCount;
            state->setParticleCount (particleCount->second.number_value());
            std::cout << particleCount->first << " : " << particleCount->second.kind_case() << "Number:" << state->getParticleCount() << std::endl;
        }

        //            auto value = frame.values().find (Keys::particleElements);
        //
        //            if ( value != frame.values().end())
        //            {
        //                auto valuept = *value;
        //                state.setParticleCount (particleCount->second.number_value());
        //                //std::cout << particleCount->first << " : " << particleCount->second.kind_case() << "Number:" << state.getParticleCount() << std::endl;
        //            }

        auto particlePositionsIt = frame.arrays().find(Keys::particlePositions);

        if (particlePositionsIt != frame.arrays().end())
        {
            auto particlePositions = particlePositionsIt->second.float_values().values();
            state->getPositionsReference().assign(particlePositions.begin(), particlePositions.end());
            std::lock_guard<std::mutex> locker(callbacksLock);
            if (onPositionUpdate)
            {
                onPositionUpdate(state->getPositionsReference());
            }
        }

        if (frame.values().contains(Keys::energyKinetic))
        {
            auto kinEnergy = frame.values().at(Keys::energyKinetic).number_value();
            //std::lock_guard<std::mutex> locker(callbacksLock);
            state->setKinEnergy(kinEnergy);
            onKinEnergyUpdate (state->getKinEnergyReference());
        }

        if (frame.values().contains(Keys::chainCount))
        {
            auto chainCount = frame.values().at(Keys::chainCount).number_value();
            state->setChainCount((int)chainCount);
            onChainCountUpdate(state->getChainCountReference());
        }      
    }
    Status status = reader->Finish();
}
