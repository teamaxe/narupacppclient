/*
 * Copyright (c) 2020 Tom Mitchell.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CommandService.h"

#include <grpcpp/grpcpp.h>
#include "narupa/protocol/command/command_service.grpc.pb.h"

//Commands
static const std::string PlaybackPlay                   = { "playback/play" };
static const std::string PlaybackReset                  = { "playback/reset" };
static const std::string PlaybackStep                   = { "playback/step" };
static const std::string PlaybacPause                   = { "playback/pause" };
static const std::string TimestepCommand                = { "sim/timestep" };
static const std::string TemperatureCommand             = { "sim/temperature" };
static const std::string FrictionCommand                = { "sim/friction" };
static const std::string NonBondedInteractionCommand    = { "sim/add_nonbonded_pair" };

//Arguments
static const std::string TemperatureArgument    = { "temperature" };
static const std::string FrictionArgument       = { "friction" };
static const std::string TimestepArgument       = { "timestep" };
static const std::string ParticleAArgument      = { "particle_a" };
static const std::string ParticleBArgument      = { "particle_b" };

class CommandMembers
{
public:
    CommandMembers() 
    {
        //connect ("130.61.91.189");
    }

    bool connect (const std::string& ipAddress)
    {
        auto ipAndPort = ipAddress + ":" + std::to_string (38801);
        channel = grpc::CreateChannel (ipAndPort, grpc::InsecureChannelCredentials());
        commandStub = narupa::protocol::command::Command::NewStub (channel);
        return true;
    }

    ~CommandMembers()
    {

    }

    bool runCommand (const std::string& commandName, const std::string& argumentName, double argumentValue)
    {
        if (commandStub == nullptr)
            return false; 

        narupa::protocol::command::CommandMessage request;
        request.set_name (commandName);

        google::protobuf::Value value;
        value.set_number_value (argumentValue);

        auto fields = request.mutable_arguments()->mutable_fields();
        (*fields)[argumentName] = value;

        narupa::protocol::command::CommandReply reply;
        grpc::ClientContext context;

        auto status = commandStub->RunCommand(&context, request, &reply);

        return (status.ok());
    }

    bool runCommand (const std::string& commandName, 
                     const std::string& argument1Name, int argument1Value, 
                     const std::string& argument2Name, int argument2Value)
    {
        narupa::protocol::command::CommandMessage request;
        request.set_name (commandName);

        google::protobuf::Value value;
        auto fields = request.mutable_arguments()->mutable_fields();

        value.set_number_value ((double)argument1Value);
        (*fields)[argument1Name] = value;

        value.set_number_value ((double)argument2Value);
        (*fields)[argument2Name] = value;

        narupa::protocol::command::CommandReply reply;
        grpc::ClientContext context;

        auto status = commandStub->RunCommand(&context, request, &reply);

        return (status.ok());
    }

    bool setTemperature (double newTemperature)
    {
        return runCommand (TemperatureCommand, TemperatureArgument, newTemperature);
    }

    bool setFriction (double newFriction)
    {
        return runCommand (FrictionCommand, FrictionArgument, newFriction);
    }

    bool setTimestep (double newTimestep)
    {
        return runCommand (TimestepCommand, TimestepArgument, newTimestep);
    }

    bool addNonbondedPair (int first, int second)
    {
        return runCommand (NonBondedInteractionCommand, ParticleAArgument, first, ParticleBArgument, second);
    }

    void printCommands()
    {
        narupa::protocol::command::GetCommandsRequest request;
        narupa::protocol::command::GetCommandsReply reply;
        grpc::ClientContext context;

        grpc::Status status = commandStub->GetCommands (&context, request, &reply);

        // Act upon its status.
        if (status.ok()) 
        {
            auto size = reply.commands_size();
            for (int i = 0; i < size; i++)
            {
                auto command = reply.commands(i);
                auto name = command.name();
                std::cout << name << std::endl;
            }
        }
        else 
        {
            std::cout << status.error_code() << ": " << status.error_message() << std::endl;
        }
    }

private:
    std::shared_ptr<grpc::Channel> channel;
    std::unique_ptr<narupa::protocol::command::Command::Stub> commandStub;
};


CommandService::CommandService()
{
    members = std::make_unique <CommandMembers>();
}

CommandService::~CommandService()
{

}

bool CommandService::connect (const std::string& ipAddress)
{
    return members->connect (ipAddress);
}

bool CommandService::setTemperature (double newTemperature)
{
    return members->setTemperature (newTemperature);
}

bool CommandService::setFriction (double newTemperature)
{
    return members->setFriction (newTemperature);
}

bool CommandService::setTimestep (double newTemperature)
{
    return members->setTimestep(newTemperature);
}

void CommandService::printCommands()
{
    members->printCommands();
}

bool CommandService::addNonbondedPair(int first, int second)
{
    return members->addNonbondedPair (first, second);
}