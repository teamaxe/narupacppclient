/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <vector>
#include <functional>
#include <string>
#include <memory>
#include <thread>
#include <atomic>
#include <mutex>

class NarupaStub;
class SimulationState;

class NarupaClient
{
public:
    NarupaClient();

    ~NarupaClient();
    void stop();

    void setPositionCallback  (std::function<void(std::vector<float>)> callback);
    void setKinEnergyCallback (std::function<void(float kinEnergy)> callback);
    void setChainCountCallback(std::function<void(int chainCount)> callback);


private:
    struct Keys
    {
        static const std::string bondPairs;         //array<uint32> pairs of bonded atoms
        static const std::string chainCount;        //int number of individual chains
        static const std::string chainNames;        //array<strings> name of above chains
        static const std::string energyKinetic;     //float system's kinetic energy
        static const std::string particleCount;     //float number atoms in the system
        static const std::string particleElements;  //array<uint32> index in the peroidic table
        static const std::string particleNames;     //array<string> array of all atom names
        static const std::string particlePositions; //array<float> position of each atom
        static const std::string particleResidues;  //array<uint32> residue to which each atom belongs
        static const std::string residueChains;     //array<uint32>
        static const std::string residueCount;      //
        static const std::string residueIds;        //array<strings> names of residues
        static const std::string systemBoxVectors;  //array<float> 9 - unit cell vector for the constraints
    };

    void run();
    void subscribeToFrames();

    std::unique_ptr <SimulationState> state;
    std::unique_ptr<NarupaStub> stub;

    std::thread thread;
    std::atomic<bool> shouldExit {false};

    std::mutex callbacksLock;
    std::function<void(std::vector<float>)> onPositionUpdate;
    std::function<void(float)> onKinEnergyUpdate;
    std::function<void(int)> onChainCountUpdate;
};
